import argparse
import os
import glob
import re
from xml.etree import ElementTree
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
import lxml.etree as etree
from update_eef import indent, write_xml


def update_aeronet_sites_checktool(write_xml, filename, directories):
    aeronets={}
    with open(filename) as aeronet_file:
        lines = aeronet_file.readlines()
        lines.pop(0)
        lines.pop(0)
        for idx, line in enumerate(lines):
            aeronets[idx]=line.rstrip().split(",")

    counts = len(aeronets)
    regex_gipp_file = re.compile('^.*CKEXT*')
    update = 0
    for dir in directories:
        directory_list = os.listdir(dir)
        for filename in directory_list:
            if regex_gipp_file.match(filename):
                filename = os.path.join(dir,filename)
                it = ElementTree.iterparse(filename)
                for _, el in it:
                    if '}' in el.tag:
                        el.tag = el.tag.split('}', 1)[1]  # strip all namespaces
                root = it.root
                for elem in root.findall(".//Data_Block/Common_EX_Parameters"):
                    try:
                        parent = elem.find("List_Of_Pixels")
                        elem.remove(parent)
                    except:
                        pass
                    item = ET.Element("List_Of_Pixels")
                    item.set("count",str(counts))
                    elem.append(item)
                    for idx, site in aeronets.items():
                        subitem = ET.SubElement(item, 'Pixel')
                        subitem.set("n",str(idx+1))
                        subitem.set("unit","degree")
                        ET.SubElement(subitem, 'Coordinate_X').text = site[1]
                        ET.SubElement(subitem, 'Coordinate_Y').text = site[2]
                        ET.SubElement(subitem, 'Freename').text = site[0]
                if update==0:
                    print("File has been updated:")
                print(filename," -> updated")
                write_xml(root,filename)
                update +=1
    if update==0:
        print("ERROR: The directory have no checktool GIPP: ",directories)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("aeronet_file")
    args = parser.parse_args()

    if os.path.isfile(args.aeronet_file):
        update_aeronet_sites_checktool(write_xml,
                args.aeronet_file, ["SENTINEL2","LANDSAT8","VENUS","SPOT"])
    else:
        print("ERROR: Please provide a valid aeronet sites file")
        exit(1)
    
