import os, sys
import pathlib
import shutil
from xml.etree import ElementTree
from datetime import datetime

# # #venus
# path_in = "/home/florian/dev/maja-gipp2/work/MACCS/MACCS/07_Echange/20240222_lut_48r1_livraison_TPI_MO/VENUS" # LIST OF LUT
# path_comp = "/home/florian/dev/maja-gipp2/VENUS_REF" # REF DEPOT GIPP
# path_out = "/home/florian/dev/maja-gipp2/VENUS_MAJA2" # OUTPUT
# mission = "VE"
# mission_full = "VENUS"
# instrument = "HRG1"
# version = "00002"
# lut_type_hdr_ref = {
#     "ALBD": "/home/florian/dev/maja-gipp2/VENUS_REF/VE_TEST_GIP_L2ALBD_L_AMMONIUM_00003_20170801_21000101.HDR",
#     "DIFT": "/home/florian/dev/maja-gipp2/VENUS_REF/VE_TEST_GIP_L2DIFT_L_AMMONIUM_00003_20170801_21000101.HDR",
#     "DIRT": "/home/florian/dev/maja-gipp2/VENUS_REF/VE_TEST_GIP_L2DIRT_L_AMMONIUM_00003_20170801_21000101.HDR",
#     "TOCR": "/home/florian/dev/maja-gipp2/VENUS_REF/VE_TEST_GIP_L2TOCR_L_AMMONIUM_00003_20170801_21000101.HDR"
# }

#sentinel2
mission = "S2B"
path_in = "/home/florian/dev/maja-gipp2/work/MACCS/MACCS/07_Echange/20240222_lut_48r1_livraison_TPI_MO/"+mission # LIST OF LUT
path_comp = "/home/florian/dev/maja-gipp2/SENTINEL2_REF" # REF DEPOT GIPP
path_out = "/home/florian/dev/maja-gipp2/SENTINEL2_NEW" # OUTPUT
mission_full = "SENTINEL2B"
instrument = "MSI"
version = "00002"

lut_type_hdr_ref = {
    "ALBD": "/home/florian/dev/maja-gipp2/SENTINEL2_REF/"+mission+"_TEST_GIP_L2ALBD_L_AMMONIUM_00002_20190710_21000101.HDR",
    "DIFT": "/home/florian/dev/maja-gipp2/SENTINEL2_REF/"+mission+"_TEST_GIP_L2DIFT_L_AMMONIUM_00002_20190710_21000101.HDR",
    "DIRT": "/home/florian/dev/maja-gipp2/SENTINEL2_REF/"+mission+"_TEST_GIP_L2DIRT_L_AMMONIUM_00002_20190710_21000101.HDR",
    "TOCR": "/home/florian/dev/maja-gipp2/SENTINEL2_REF/"+mission+"_TEST_GIP_L2TOCR_L_AMMONIUM_00002_20190710_21000101.HDR"
}

path_in =pathlib.Path(path_in)
path_comp =pathlib.Path(path_comp)
path_out =pathlib.Path(path_out)

date_begin = "20190710"
date_end = "21000101"

# SPOT4
# date_begin = "19980324"
# date_end = "20130801"

solar_index = "0.0 7.5 15.0 22.5 30.0 37.5 45.0 52.5 60.0 67.5 75.0" #
view_index = "0.0 7.0 14.0 21.0 27.0 33.0 39.0 45.0 50.0 56.0 60.0" #
relative_azimuth = "0.0 30.0 60.0 90.0 120.0 150.0 180.0" #
altitude_index = "0 1000 2000 3000" #
aot_index = "0.0 0.0625 0.125 0.1875 0.25 0.3125 0.375 0.4375 0.5 0.5625 0.625 0.6875 0.75 0.8125 0.875 0.9375 1.0 1.0625 1.125 1.1875 1.25 1.3125 1.375 1.4375 1.5 2.0 3.0" #

toa_index = "-0.2 -0.13 -0.06 0.01 0.08 0.15 0.22 0.29 0.36 0.43 0.5 0.57 0.64 0.71 0.78 0.85 0.92 0.99 1.06 1.13"

# TODO mettre un fichier HDR de chaque type de lut

# Faire une liste des type de LUT avec fichiers associé
# Faire une liste des especes







lut_type_filename_prefix = {
    "ALBD": "albedo",
    "DIFT": "Tdif",
    "DIRT": "Tdir",
    "TOCR": "lutInvCS"
}
# key: espece name in dir/HDR, value: lut filename
espece_dict = {
    "AMMONIUM": "Ammonium",
    "BLACKCAR": "BlackCarbon",
    "DUST": "Dust",
    "DUST48R1": "Dust48r1",
    "NITRATE": "Nitrate",
    "ORGANICM": "OrganicMatter",
    "ORGA48R1": "OrganicMatter48r1",
    "SEASALT": "SeaSalt",
    "SECONDAR": "SOA",
    "SULPHATE": "Sulphate",
    "SULP48R1": "Sulphate48r1",
    "CONTINEN": "zerodeux"
}

# lut filename: TYPELUT_V51_CAPTEUR_ESPECE(RHXX)(CAMS)_bande


def sort_path(name):
    if int(str(name).split("_")[-1]) > 1000:
        return '_'.join(str(name).split("_")[:-1]+["a"+str(name).split("_")[-1]])
    else:
        return str(name)


def build_gipp():
    total_file = 0
    hdr_not_found = []

    for esp in espece_dict.keys():
        for lut_type in lut_type_hdr_ref.keys():

            lut_prefix = lut_type_filename_prefix[lut_type]
            esp_name = espece_dict[esp]
            esp_eight = esp + "_"*(8-len(esp))

            print(f"Build LUT for {esp}({esp_name}) of type {lut_type}({lut_prefix})")

            if esp in ["DUST", "DUST48R1", "BLACKCAR"]:
                search_pattern = f"*{lut_prefix}*{esp_name}CAMS*"
            elif esp == "CONTINEN":
                search_pattern = f"*{lut_prefix}*{esp_name}*"
            else:
                search_pattern = f"*{lut_prefix}*{esp_name}RH*"

            print(f"\tSearch pattern: {search_pattern}")
            list_file = set((path_in.glob(search_pattern)))

            list_txt_file = set((path_in.glob(search_pattern+".txt")))
            list_lut_file = list_file - list_txt_file

            # for i in list_lut_file:
            #     print(i)

            list_lut_file = sorted(list_lut_file)
            list_txt_file = sorted(list_txt_file)
            # print()

            # for i in list_lut_file:
            #     print(i)
            it_lut = enumerate(list_lut_file)
            if mission_full == "VENUS":
                for id,lut_name in it_lut:
                    # Pour venus il y a deux bandes 619 ! à dupliquer
                    if "bande_619" in str(lut_name):
                        list_lut_file.insert(id, lut_name)
                        next(it_lut)


            print(f"\tLUT files found {len(list_lut_file)}")
            print(f"\tTXT files found {len(list_txt_file)}")
            list_lut_name = [f.name for f in list_lut_file]
            list_lut_name_sorted = []

            print(list_lut_name[0])
            if "RH" in list_lut_name[0]:
                print("RH TRUE")
                RH_LIST = ["30", "50", "70", "80", "85", "90", "95"]
                for rh in RH_LIST:

                    set((path_in.glob(search_pattern + ".txt")))
                    rh_luts = sorted([f for f in list_lut_name if "RH"+rh+"CAMS" in f], key=lambda x:int(x.split("_")[-1]))
                    # print(rh_luts)
                    list_lut_name_sorted += rh_luts
            else:
                print("RH FALSE")
                list_lut_name_sorted = sorted([f for f in list_lut_name], key=lambda x:int(x.split("_")[-1]))

            # for f in list_lut_name_sorted:
            #     print(f)

            if len(list_lut_name_sorted) != len(list_lut_file):
                print("list_lut_name_sorted != list_lut_file error!")
                exit()

            total_file += len(list_lut_file)
            total_file += len(list_txt_file)

            # for f in list_lut_file[:]:
            #     print("\t\t"+str(f))
            # for f in list_txt_file[:]:
            #     print("\t\t"+str(f))

            # HDR
            # Find existing
            regex = f"{mission}_TEST_GIP_L2{lut_type}_L_{esp_eight}*HDR"
            hdr_find = list(path_comp.glob(regex)) #"dir_hdr_name+".HDR")
            # print(hdr_file)
            # print(hdr_file.is_file())
            if len(hdr_find) != 0:
                hdr_file = hdr_find[0]
                print(f"\tFind old associated HDR: {hdr_file}")
            # ... or get default one
            elif len(hdr_find) > 1:
                print(f"ISSUE {regex}")
                exit()
            else:
                hdr_not_found.append(regex)
                hdr_file = lut_type_hdr_ref[lut_type]
                print(f"\tCannot find associate HDR, Get default HDR {regex}")


            ElementTree.register_namespace("","http://eop-cfi.esa.int/CFI")
            it = ElementTree.parse(hdr_file)

            ns = {"xx": "http://eop-cfi.esa.int/CFI"}
            if hdr_find:
                version = it.findall(".//xx:File_Version", ns)[0].text
                version = str(int(version) + 1)
                version = str("0"*(4 - len(version)) + version)

                # date_begin =
            else:
                version = "0001"
            print(version)

            # CREATE LUT DIR AND COPY LUT AND TXT FILES

            dir_hdr_name = f"{mission}_TEST_GIP_L2{lut_type}_L_{esp_eight}_0{version}_{date_begin}_{date_end}"
            new_d = path_out / (dir_hdr_name + ".DBL.DIR")
            new_d.mkdir(parents=True, exist_ok=True)
            print(f"\tCreate LUT DIR and copy lut and txt files: {new_d.name}")

            for f in (list_lut_file+list_txt_file):
                shutil.copy(f, new_d/f.name)

            it.findall(".//xx:File_Name", ns)[0].text = dir_hdr_name
            it.findall(".//xx:Mission", ns)[0].text = f"{mission_full}"
            # TODO increment file version: faire lien avec nom fichier et dossier
            it.findall(".//xx:File_Version", ns)[0].text = f"{version}"

            it.findall(".//xx:Validity_Start", ns)[0].text = f"UTC={date_begin[:4]}-{date_begin[4:6]}-{date_begin[6:8]}" \
                                                             f"T00:00:00"
            it.findall(".//xx:Validity_Stop", ns)[0].text = f"UTC={date_end[:4]}-{date_end[4:6]}-{date_end[6:8]}" \
                                                            f"T00:00:00"
            it.findall(".//xx:Validity_Start", ns)[1].text = f"UTC={date_begin[:4]}-{date_begin[4:6]}-{date_begin[6:8]}" \
                                                             f"T00:00:00"
            it.findall(".//xx:Validity_Stop", ns)[1].text = f"UTC={date_end[:4]}-{date_end[4:6]}-{date_end[6:8]}" \
                                                            f"T00:00:00"
            date_utc = str(datetime.utcnow())
            date_utc = f"UTC={date_utc[:10]}T{date_utc[11:19]}"
            it.findall(".//xx:Creation_Date", ns)[0].text = f"{date_utc}"

            node_list = it.findall(".//xx:List_of_Packaged_DBL_Files", ns)[0]

            node_list.set("count", str(len(list_lut_name_sorted)))

            for i in range(len(list_lut_name_sorted)):
                dbl = node_list.find(f".//xx:Packaged_DBL_File[@sn='{i + 1}']", ns)
                fp = dbl.find(".//xx:Relative_File_Path", ns)
                fp.text = f"{dir_hdr_name}.DBL.DIR/{list_lut_name_sorted[i]}"

            for i in range(len(list_lut_name_sorted), len(node_list)):
                dbl = node_list.find(f".//xx:Packaged_DBL_File[@sn='{i + 1}']", ns)
                node_list.remove(dbl)

            # Add index informations
            # list_index = {"Solar_Zenith_Angle_Indexes": solar_index, "View_Zenith_Angle_Indexes": view_index,
            #               "Relative_Azimuth_Angle_Indexes": relative_azimuth, "Altitude_Indexes": altitude_index,
            #               "AOT_Indexes": aot_index, "TOA_Reflectance_Indexes": toa_index}
            # index_write = []
            # for k in list_index.keys():
            #     n = it.find(f".//xx:{k}", ns)
            #     if n is not None:
            #         n.text = list_index[k]
            #         index_write.append(k)
            # print("Add following index:", index_write)

            it.write(path_out / f"{dir_hdr_name}.HDR",
                     encoding="UTF-8", xml_declaration=True)

        # exit()
        print("\n\n")
    print(total_file)
    print(f"Cant found hdr for {len(hdr_not_found)} files")
    for f in hdr_not_found:
        print(f)



def build_spot_gipp():
    # On regarde tous les dossiers DBL.DIR

    for d in sorted(path_comp.glob("S2A_TEST_GIP*DBL.DIR")):

        #######################
        ######## LUT ##########
        #######################
        print(d)
        txt = list(d.glob("*"))
        if len(txt) == 0:
            print("no file")
            continue
        if "L2WATV" in d.name:
            print("skip L2WATV")
            continue

        # On prend le premier fichier lut dans le dossier
        txt = txt[0]
        name = txt.name

        # Extraction du type de LUT
        gipp_type = name.split("_V51_")[0]
        if gipp_type == "lut_inv_CS":
            gipp_type = "lutInvCS"

        # On recupere l'espece
        var = name.split("_V51_")[1].split("_")[1]
        if var == "Sulfate":
            var = "Sulphate"
        print(f"Search of {gipp_type} and {var}:", end="")
        # for p in path_in.glob(f"*{gipp_type}*{var}*"):
        #     print(f"\t{p}")

        # On reucpere les LUT pour le type et l'espece
        list_file = list(path_in.glob(f"*{gipp_type}*{var}*"))
        list_file_txt = list(path_in.glob(f"*{gipp_type}*{var}*.txt"))

        if len(list_file) == 0:
            print("NO FILE FOUND")
            continue
        print(len(list_file))
        continue


        ds = str(d.name).split("_")
        # type LUT L2ABD
        ltype = ds[3]
        # nom espece maja LUT continent
        lvar = ds[5]

        if "DUST" in d.name:
            lvar += "____"
        elif "NITRATE" in d.name or "SEASALT" in d.name:
            lvar += "_"
        new_d = path_out / f"{mission}_TEST_GIP_{ltype}_L_{lvar}_{version}_{date_begin}_{date_end}.DBL.DIR"
        new_d.mkdir(parents=True, exist_ok=True)
        # print((path_out/(f"SPOT1_TEST_GIP_{ltype}_L_{lvar}_00002_20190710_21000101.DBL.DIR")))

        for f in list_file:
            shutil.copy(f, new_d/f.name)
            # print(f"{f} to {new_d/f.name}")

        #######################
        ######## HDR ##########
        #######################

        # Création fichier hdr
        print(d.name[:-8])
        hdr_file = list(path_comp.glob(f"{d.name[:-8]}*.HDR"))
        if len(hdr_file) == 0:
            print("NO HDR FILE FOUND")
        hdr_file = hdr_file[0]


        ElementTree.register_namespace("","http://eop-cfi.esa.int/CFI")
        it = ElementTree.parse(hdr_file)


        ns = {"xx":"http://eop-cfi.esa.int/CFI"}
        it.findall(".//xx:File_Name", ns)[0].text = f"{mission}_TEST_GIP_{ltype}_L_{lvar}_" \
                                                    f"{version}_{date_begin}_{date_end}"
        it.findall(".//xx:Mission", ns)[0].text = f"{mission_full}"
        # TODO increment file version: faire lien avec nom fichier et dossier
        it.findall(".//xx:File_Version", ns)[0].text = f"{version}"

        it.findall(".//xx:Validity_Start", ns)[0].text = f"UTC={date_begin[:4]}-{date_begin[4:6]}-{date_begin[6:8]}" \
                                                         f"T00:00:00"
        it.findall(".//xx:Validity_Stop", ns)[0].text = f"UTC={date_end[:4]}-{date_end[4:6]}-{date_end[6:8]}" \
                                                        f"T00:00:00"
        it.findall(".//xx:Validity_Start", ns)[1].text = f"UTC={date_begin[:4]}-{date_begin[4:6]}-{date_begin[6:8]}" \
                                                         f"T00:00:00"
        it.findall(".//xx:Validity_Stop", ns)[1].text = f"UTC={date_end[:4]}-{date_end[4:6]}-{date_end[6:8]}" \
                                                        f"T00:00:00"
        date_utc = str(datetime.utcnow())
        date_utc = f"UTC={date_utc[:10]}T{date_utc[11:19]}"
        it.findall(".//xx:Creation_Date", ns)[0].text = f"{date_utc}"


        node_list = it.findall(".//xx:List_of_Packaged_DBL_Files", ns)[0]

        list_file_wtxt = sorted(list(set(list_file) - set(list_file_txt)), key=sort_path)
        # print("len")
        # print(len(list_file))
        # print(len(list_file_txt))
        # print(len(list_file_wtxt))

        # print(node_list.findall(f".//xx:Packaged_DBL_File",ns))
        node_list.set("count", str(len(list_file_wtxt)))

        for i in range(len(list_file_wtxt)):
            # print(node_list.find(f".//xx:Packaged_DBL_File[@sn='1']",ns))
            dbl = node_list.find(f".//xx:Packaged_DBL_File[@sn='{i+1}']",ns)
            # print(dbl)
            fp = dbl.find(".//xx:Relative_File_Path", ns)
            # print(dbl.attrib["sn"],fp.text)
            fp.text =  f"{mission}_TEST_GIP_{ltype}_L_{lvar}_{version}_{date_begin}_{date_end}.DBL.DIR/{list_file_wtxt[i].name}"
            # print(fp.text)

        for i in range(len(list_file_wtxt),len(node_list)):
            # print(i)
            dbl = node_list.find(f".//xx:Packaged_DBL_File[@sn='{i + 1}']", ns)
            node_list.remove(dbl)


        # Add index informations
        list_index = {"Solar_Zenith_Angle_Indexes": solar_index, "View_Zenith_Angle_Indexes": view_index,
                      "Relative_Azimuth_Angle_Indexes": relative_azimuth, "Altitude_Indexes": altitude_index,
                      "AOT_Indexes": aot_index, "TOA_Reflectance_Indexes": toa_index}
        index_write = []
        for k in list_index.keys():
            n = it.find(f".//xx:{k}", ns)
            if n is not None:
                n.text = list_index[k]
                index_write.append(k)
        print("Add following index:",index_write)

        it.write(path_out / f"{mission}_TEST_GIP_{ltype}_L_{lvar}_{version}_{date_begin}_{date_end}.HDR",
                 encoding="UTF-8", xml_declaration=True)



        print()


def comp_in_out():
    count = 0
    for f in sorted(list(path_in.glob("*"))):
        exist = False
        count += 1
        for d in path_out.glob("*"):
            # print(d,f.name)
            if len(list(d.glob(f.name))) != 0:
                exist = True
                break

        if not exist:
            print(f.name, "not in out")
        # else:
        #     print(f.name, "in out")
        # break
    print(count)



# path_in = "/home/afiche/Documents/Data/SWH/test_spot5_hrg2/"
# path_in = pathlib.Path(path_in)
#
# res = list(path_in.glob("SPOT*GIP*HDR"))
# print(len(res))
#
# for p in res:
#     print(p)
#     ElementTree.register_namespace("", "http://eop-cfi.esa.int/CFI")
#     it = ElementTree.parse(p)
#     it.findall(".//xx:File_Version", {"xx": "http://eop-cfi.esa.int/CFI"})[0].text = '0001'
#     it.write(p,encoding="UTF-8", xml_declaration=True)
#
# print("test")
# comp_in_out()
# build_spot_gipp()

build_gipp()

