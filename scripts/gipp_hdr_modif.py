import os

import argparse
from shutil import copyfile


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("indir")
    args = parser.parse_args()

    os.system(
        "for file in `ls "+args.indir +"| grep '.HDR'`; do a=${file%.*}; sed -i 's#<File_Name>\(.*\)</File_Name>#<File_Name>'$a'</File_Name>#' "+args.indir+"$a.HDR; done")

    os.system(
        "for file in `ls " + args.indir + "| grep '.HDR'`; do a=${file%.*}; sed -i 's#<Validity_Start>\(.*\)</Validity_Start>#<Validity_Start>UTC=2019-06-26T00:00:00</Validity_Start>#' " + args.indir + "$a.HDR; done")

    os.system(
        "for file in `ls " + args.indir + "| grep '.HDR'`; do a=${file%.*}; sed -i 's#<Validity_Stop>\(.*\)</Validity_Stop>#<Validity_Stop>UTC=2100-01-01T00:00:00</Validity_Stop>#' " + args.indir + "$a.HDR; done")

    os.system(
        "for file in `ls " + args.indir + "| grep '.HDR'`; do a=${file%.*}; sed -i 's#<Creation_Date>\(.*\)</Creation_Date>#<Creation_Date>UTC=2019-06-26T00:00:00</Creation_Date>#' " + args.indir + "$a.HDR; done")


    os.system(
        "for file in `ls " + args.indir + "| grep '.HDR'`; do a=${file%.*}; sed -i 's#<File_Version>\(.*\)</File_Version>#<File_Version>0001</File_Version>#' " + args.indir + "$a.HDR; done")

    os.system(
        "for file in `ls " + args.indir + "| grep '.HDR'`; do a=${file%.*}; sed -i 's#<Applicability_NickName>\(.*\)</Applicability_NickName>#<Applicability_NickName>ALLSITES</Applicability_NickName>#' " + args.indir + "$a.HDR; done")


