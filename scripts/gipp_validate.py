import os

import argparse,sys,shlex,subprocess
from threading import Thread
try:
    from queue import Queue, Empty
except ImportError:
    from Queue import Queue, Empty  # python 2.x

from xml.dom import minidom

def get_schema_location(dom):
    lXsdFilename = None
    for i in range(dom.attributes.length):
        attrib = dom.attributes.item(i)
        if "schemaLocation" in attrib.name:
            lXsdFilename = attrib.value
            break
        if lXsdFilename is None:
            if "noNamespaceSchemaLocation" in attrib.name:
                lXsdFilename = attrib.value
                break
    if lXsdFilename is None or len(lXsdFilename) == 0:
        return None

    # --------------------------------------
    # Ex: lXsdFilename = xsi:schemaLocation="http:#eop-cfi.esa.int/CFI
    # ../../Schemas/Venus/VE_GIP_CKQLTL_QuicklookTool.xsd"
    # Extract the name of the Xsd file
    lXsdFilename = str(lXsdFilename)
    listString = lXsdFilename.split(' ')
    # Get the Latest value
    lTheXsdFilename = ""
    nbStrings = len(listString)
    # Loop under strings
    for i in range(0, nbStrings):
        lTheXsdFilename = listString[i]
        lTheXsdFilename = lTheXsdFilename.replace("\\", "/")
        lTheXsdFilename = lTheXsdFilename.replace("//", "/")
        if "xsd" in os.path.splitext(lTheXsdFilename)[1]:
            lTheXsdFilename = os.path.basename(lTheXsdFilename)
    return lTheXsdFilename

def launch_command(command):
    # Convert cmd from string to list
    if isinstance(command, str):
        if sys.platform == "win32":
            lex = shlex.shlex(command, posix=False)
        else:
            lex = shlex.shlex(command)
        lex.quotes = '"'
        lex.whitespace_split = True
        cmd_name = list(lex)
    elif isinstance(command, list):
        cmd_name = command
    cmd_name = " ".join([str(x) for x in cmd_name])
    # Indeed launch command

    def enqueue_output(out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()

    err_queue = Queue()
    out_queue = Queue()
    try:
        p = subprocess.Popen(cmd_name, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        err_t = Thread(target=enqueue_output, args=(p.stderr, err_queue))
        out_t = Thread(target=enqueue_output, args=(p.stdout, out_queue))
        out_t.daemon = True
        err_t.daemon = True
        out_t.start()
        err_t.start()
    except OSError as err:
        print('An error occured with command : %s with message %s', cmd_name, err)
        return err.errno

    while True:
        try:
            err_line = err_queue.get_nowait()
        except Empty:
            err_line = None
        try:
            out_line = out_queue.get_nowait()
        except Empty:
            out_line = None
        # Test if process is active
        status = p.poll()
        if status is not None:
            if (out_line is None) and (err_line is None):
                print("End of command")
                break
        if out_line is not None:
            print(out_line)
        if err_line is not None:
            print(err_line)

    return p.returncode

def check_xml(xml_file, schema_location=None, xsd_file=None):
    """
    #TODO: to be implemented
    :param xml_file:
    :param xsd_file:
    :return:
    """

    # -----------------------
    # Load the xml file
    dom_doc = minidom.parse(xml_file)
    dom_node = dom_doc.documentElement
    

    if xsd_file is None:
        if schema_location is None:
            raise Exception("Either schema path or schema location needed to validate xml")
        # --------------------------------------
        # Get the schema location
        lXsdFilename = get_schema_location(dom_node)
        if lXsdFilename is None:
            raise Exception(
                "Error while reading the attribute 'xsi:schemaLocation' or 'xsi:noNamespaceSchemaLocation' " +
                "in the xml file " + xml_file + ". Impossible to check the file with its schema!")

        print("The Xsd Filename <" + lXsdFilename + "> had been detected in the xml file <" + xml_file + ">.")

        # --------------------------------------
        # Get the full path of the schemalocation
        lFullXsdFilename = os.path.join(schema_location, lXsdFilename)
    else:
        lFullXsdFilename = xsd_file

    # --------------------------------------
    # Check the Xsd to the xml file
    result = launch_command("xmllint --noout " + xml_file + " --schema " + lFullXsdFilename)
    if result != 0:
        raise Exception(
            "The xml file <" + xml_file + "> is not conform with its schema <" + lFullXsdFilename + "> ! ")
    # --------------------------------------
    print("The XML file <" + xml_file + "> is conform with its schema.")

    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("schemasdir")
    args = parser.parse_args()

    if not os.path.exists(args.input):
        print("File or dir : "+args.input+" does not exist")
        exit(1)
              

    if os.path.isfile(args.input):
        check_xml(args.input, args.schemasdir)
    else:
         files = [os.path.join(args.input,f) for f in os.listdir(args.input) if f.endswith('.EEF') or f.endswith('.HDR')]
         for f in files:
             print("File: %s" % f)
             check_xml(f, args.schemasdir)
    exit(0)
        

