# Ground Image Processing Parameters for Maja

MAJA uses GIPP-files (Ground Image Processing Parameters) to configure
the different algorithms of the chain without having to recompile the
code.

This repository contains : 
- for each mission:
    - the official GIPP for MAJA (“muscate” and native
formats compliant). You are free to
clone it and modify the parameters if you want to change the behavior
of the processing chain. By default, the CAMS option is enable, to disable
this you have to turn off the Use_Cams_Data field in the L2COMM Gipp. 
    - LUTS (“muscate” and native
formats compliant too) for each selected model in the Model_List node of L2COMM : TOCR, DIRT, DIFT and ALBD. If the value of the "Use_Cams_Data" field is false the CONTINEN model will be used instead. 
- schema folder : contains all the schema used to validate the gipp files for each mission. 
- scripts folder : python scripts for gipps maintenance. 
    - If you want to validate your modifications you can directly use the script gipp_validate.py:

    ```
    Example : python3 scripts/gipp_validate.py SENTINEL2 schemas/SENTINEL2
    ```
    - To update Aeronet sites you should use the script update_checktools.py:

    ```
    Example: python3 scripts/update_checktools.py new_aeronet_sites_file
    ``` 
