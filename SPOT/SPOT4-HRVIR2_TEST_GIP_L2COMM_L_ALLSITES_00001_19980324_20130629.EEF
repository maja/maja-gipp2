<?xml version="1.0" encoding="UTF-8"?>
<Earth_Explorer_File xmlns="http://eop-cfi.esa.int/CFI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="L2COMM_File_Type" schema_version="1.00" xsi:schemaLocation="http://eop-cfi.esa.int/CFI ./GIP_L2COMM_L2Commons.xsd">
  <Earth_Explorer_Header>
    <Fixed_Header>
      <File_Name>SPOT4-HRVIR2_TEST_GIP_L2COMM_L_ALLSITES_00001_19980324_20130629</File_Name>
      <File_Description>L2CommonParameters</File_Description>
      <Notes/>
      <Mission>SPOT4</Mission>
      <File_Class>TEST</File_Class>
      <File_Type>GIP_L2COMM</File_Type>
      <Validity_Period>
        <Validity_Start>UTC=1998-03-01T00:00:00</Validity_Start>
        <Validity_Stop>UTC=2013-08-01T00:00:00</Validity_Stop>
      </Validity_Period>
      <File_Version>0001</File_Version>
      <Source>
        <System>MAJA</System>
        <Creator>CNES_tool</Creator>
        <Creator_Version>1.0</Creator_Version>
        <Creation_Date>UTC=2022-05-19T00:00:00</Creation_Date>
      </Source>
    </Fixed_Header>
    <Variable_Header>
      <Main_Product_Header>
        <List_of_Consumers count="0"/>
        <List_of_Extensions count="0"/>
      </Main_Product_Header>
      <Specific_Product_Header>
        <Instance_Id>
          <Applicability_NickName_Type>L_LIST</Applicability_NickName_Type>
          <Applicability_NickName>ALLSITES</Applicability_NickName>
          <Serial_Key>00001</Serial_Key>
          <Validity_Period>
            <Validity_Start>UTC=1998-03-01T00:00:00</Validity_Start>
            <Validity_Stop>UTC=2013-08-01T00:00:00</Validity_Stop>
          </Validity_Period>
        </Instance_Id>
        <List_of_Applicable_SiteDefinition_Ids count="0"/>
      </Specific_Product_Header>
    </Variable_Header>
  </Earth_Explorer_Header>
  <Data_Block>
    <Common_Parameters>
      <!-- No data value -->
      <No_Data>-1000</No_Data>
      <!-- Maximum percentage of NoData pixels for the product to be considered as valid -->
      <Max_No_Data_Percentage>99</Max_No_Data_Percentage>
      <!-- Maximum percentage of cloudy pixels for the product to be considered as valid (in percentage) -->
      <Max_Cloud_Percentage>90</Max_Cloud_Percentage>
      <!-- Type of interpolator used in the LUT manipulation -->
      <LUT_Interpolation>LINEAR</LUT_Interpolation>
      <!-- Minimum value of the cosine of incidence angle (to avoid to normalise reflectances with negative values or close to infinite values) -->
      <!-- This parameter is used in Slope Correction -->
      <Slope_Min_Cos_I>0.20</Slope_Min_Cos_I>
      <!-- L2/L3 specifics products configuration values -->
      <VAP_Configuration_Values>
        <!--VAP quantification value -->
        <VAP_Quantification_Value>0.050</VAP_Quantification_Value>
        <!--VAP Nodata value (in [0;255]) -->
        <VAP_No_Data_Value>0</VAP_No_Data_Value>
      </VAP_Configuration_Values>
      <AOT_Configuration_Values>
        <!--AOT quantification value -->
        <AOT_Quantification_Value>0.005</AOT_Quantification_Value>
        <!--AOT Nodata value (in [0;255]) -->
        <AOT_No_Data_Value>0</AOT_No_Data_Value>
      </AOT_Configuration_Values>
      <Cal_Adjust_Option>false</Cal_Adjust_Option>
      <Cal_Adjust_Factor>1.0 1.0 1.0 1.0</Cal_Adjust_Factor>
      <!-- Constant model to use in case of absent/invalid CAMS -->
      <Constant_Model>CONTINEN</Constant_Model>
    </Common_Parameters>
    <Bands_Definition>
      <Thematic_Definition>
        <!-- Green band code identifier -->
        <Green_Band_Code>XS1</Green_Band_Code>
        <!-- Red band code identifier -->
        <Red_Band_Code>XS2</Red_Band_Code>
        <!-- Near Infra Red band code identifier -->
        <NIR_Band_Code>XS3</NIR_Band_Code>
        <!--  SWIR band code identifier -->
        <SWIR_Band_Code>SWIR</SWIR_Band_Code>
        <!-- Band code identifier used for the red channel of the quick look -->
        <Quicklook_Red_Band_Code>XS3</Quicklook_Red_Band_Code>
        <!-- Band code identifier used for the green channel of the quick look -->
        <Quicklook_Green_Band_Code>XS2</Quicklook_Green_Band_Code>
        <!-- Band code identifier used for the blue channel of the quick look -->
        <Quicklook_Blue_Band_Code>XS1</Quicklook_Blue_Band_Code>
      </Thematic_Definition>
    </Bands_Definition>
    <Quicklook_Refl_Variation>
      <!-- Minimum reflectance value in the red channel takes in care for the quick look rescaling -->
      <Min_Ref_Red_Band>0.000</Min_Ref_Red_Band>
      <!-- Maximum reflectance value in the red channel takes in care for the quick look rescaling -->
      <Max_Ref_Red_Band>0.500</Max_Ref_Red_Band>
      <!-- Minimum reflectance value in the greeb channel takes in care for the quick look rescaling -->
      <Min_Ref_Green_Band>0.000</Min_Ref_Green_Band>
      <!-- Maximum reflectance value in the green channel takes in care for the quick look rescaling -->
      <Max_Ref_Green_Band>0.250</Max_Ref_Green_Band>
      <!-- Minimum reflectance value in the blue channel takes in care for the quick look rescaling -->
      <Min_Ref_Blue_Band>0.000</Min_Ref_Blue_Band>
      <!-- Maximum reflectance value in the blue channel takes in care for the quick look rescaling -->
      <Max_Ref_Blue_Band>0.250</Max_Ref_Blue_Band>
    </Quicklook_Refl_Variation>
    <Subsampling>
      <!-- Threshold to consider a L2 resolution pixel as saturated -->
      <Saturation_Threshold>1.3</Saturation_Threshold>
      <!-- Threshold to consider a L2 coarse resolution image as saturated -->
      <Saturation_Threshold_Sub>0.900</Saturation_Threshold_Sub>
    </Subsampling>
    <CAMS_Data_Preparation>
      <!-- Boolean to activate CAMS -->
      <Use_Cams_Data>true</Use_Cams_Data>
      <!-- List of model to use in cams -->
      <List_Of_Models>SULPHATE DUST SEASALT ORGANICM BLACKCAR</List_Of_Models>
      <!-- RH sampling for LUT computation -->
      <RH_Sampling>30 50 70 80 85 90 95</RH_Sampling>
      <!-- Proportion of optical thickness -->
      <limAOT>0.95</limAOT>
      <!-- Number of hours defining the temporal window for CAMS Data validity -->
      <Time_Windows_CAMS>12</Time_Windows_CAMS>
    </CAMS_Data_Preparation>
    <ERA5_Data_Preparation>
      <!-- Number of hours defining the temporal window for ERA5 specific humidities Data validity -->
      <Time_Windows_ERA5>3</Time_Windows_ERA5>
    </ERA5_Data_Preparation>
    <DTM_Processing>
      <!-- Euclidean distance beyong which a pixel is declared as hidden (in meter) -->
      <Distance_Threshold>100.00</Distance_Threshold>
    </DTM_Processing>
    <Atmospheric_Absorption_Correction>
      <!-- Option to use the default constant water vapour amount (if false use the GIP_L2WATV) -->
      <Use_Default_Constant_Water_Amount>false</Use_Default_Constant_Water_Amount>
      <Ozone_Amount_Default_Value>0.330</Ozone_Amount_Default_Value>
      <!-- Water amount value (used if Use_Water_Amount set to true) -->
      <Water_Amount_Default_Value>2</Water_Amount_Default_Value>
    </Atmospheric_Absorption_Correction>
    <Rayleigh_Correction>
      <!-- Default value of AOT for the Rayleigh correction -->
      <Rayleigh_Correction_Default_AOT>0.05</Rayleigh_Correction_Default_AOT>
    </Rayleigh_Correction>
    <Geometric_Flags>
      <!-- Maximum angular distance to sunglint under which a viewing direction is declared close to sunglint (in degree) -->
      <Sun_Glint_Threshold unit="deg">30</Sun_Glint_Threshold>
      <!-- Maximum angular distance to hotspot under which a viewing direction is declared close to hotspot (in degree) -->
      <Hot_Spot_Threshold unit="deg">5</Hot_Spot_Threshold>
    </Geometric_Flags>
    <AOT_Estimation>
      <AOT_HeightScale>2000</AOT_HeightScale>
      <!-- Used AOT value if AOT cannot be estimated -->
      <Default_AOT>0.10</Default_AOT>
    </AOT_Estimation>
    <Environment_Correction>
      <!-- option to apply the environment correction -->
      <Env_Corr_Option>true</Env_Corr_Option>
      <!-- Convolution radius -->
      <Rho_Env_Stddev>3.3</Rho_Env_Stddev>
    </Environment_Correction>
    <Slope_Correction>
      <!-- Minimum value of the cosine of exitence angle (to avoid a negative BRDF factor) -->
      <Min_Cos_E>0.10</Min_Cos_E>
      <!-- Minimum the cosine ratio value -->
      <Min_Cos_Ratio>0.20</Min_Cos_Ratio>
    </Slope_Correction>
  </Data_Block>
</Earth_Explorer_File>
