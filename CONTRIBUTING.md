# How to push changes ?
## Reporting bug
If you have found a bug, you can first search [the existing issues](https://gitlab.orfeo-toolbox.org/maja/maja-gipp2/-/issues?scope=all&state=all) to see if it has already been reported. If not, please [open a new issue](https://gitlab.orfeo-toolbox.org/maja/maja-gipp2/-/issues/new?issue%5Bmilestone_id%5D=) on GitLab.


## Merge Request
Your contribution is ready to be added to the main repository? Send a Merge
Request against the `develop` branch on GitLab using the merge request
template. When your MR is ready and the CI is green, add the label "~ReadyForReview", 
the maintainers of this projet will review and merge to develop. 

## GitLab guidelines
### Labels
Regarding labels, we use the following set:
* ~"To Do": action is planned
* ~Doing: Work in progress
* ~"To be verified by CNES": Verification to be done by CNES
* ~Accepted: Issue correction accepted by CNES. To be closed by QA.
* ~ReadyForReview : Review will be done by CS before merging
