FROM rockylinux:8

LABEL VERSION="1.0" ARCHITECTURE="amd64"
LABEL MAINTAINER "MAJA development team"

RUN dnf -y update \
&& dnf -y install libxml2 \
    python3 \
&& dnf clean all
